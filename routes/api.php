<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('login', 'Api\v1\UserController@login');
Route::post('login', 'Api\v1\UserController@login');
Route::post('register', 'Api\v1\UserController@register');


Route::group(['prefix'=>'v1','middleware' => 'auth:api'], function(){

	Route::post('details', 'Api\v1\UserController@details');

	Route::get('berita', 'Api\v1\BeritaController@index');
	Route::get('berita/{article}', 'Api\v1\BeritaController@show');
	Route::post('berita', 'Api\v1\BeritaController@store');
	Route::put('berita/{article}', 'Api\v1\BeritaController@update');
	Route::delete('berita/{article}', 'Api\v1\BeritaController@delete');

});
 