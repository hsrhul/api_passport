<?php

namespace App\Http\Controllers\Api\v1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Model\Berita;
use Response;
use App\Helpers\Helper;

class BeritaController extends Controller
{
    public function index()
    {
        $model = Berita::all();
        $tgl = Helper::tgl('12-30-2017');
        return response()->json([
                    'status'=>'200',
                    'tgl'=>$tgl, 
                    'data'=>$model, 
                ]);
    }

    public function create()
    {
    }

    public function store(Request $request)
    {
        $model = $request->all();
        $foto = isset($model['foto']) ? $model['foto'] : '';
        $model['foto'] = Helper::uploadImage($foto, 'file/berita/', '');

        if (Berita::create($model)){ 
            return response()->json([
                    'status'=>'200',
                    'msg'=>'Data Berhasil Disimpan', 
                ]);
        }else{
            return response()->json([
                    'status'=>'400', 
                    'msg'=>'Data Gagal Disimpan', 
                ]);
        }
    }

    public function show($id)
    {
    }

    public function edit($id)
    {
    }

    public function update(Request $request, $id)
    {
    }

    public function destroy($id)
    {
    }
}
