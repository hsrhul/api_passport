<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Berita extends Model
{
    protected $table    = 'berita';
    protected $primaryKey = 'id';
    // public $incrementing = false;
    protected $fillable = [ 
       'judul', 
       // 'kategori', 
       // 'deskripsi', 
       'isi', 
       // 'headline', 
       // 'tag', 
       // 'rating', 
       'foto', 
       // 'user', 
    ];
}
