<?php
namespace App\Helpers;
use Validator;
use Image;
use File;
use DB;

class Helper { 

	// tahun - bulan -tanggal
	public static function tgl($tgl) {
		$tanggal = substr($tgl,0,2);
		$bulan = substr($tgl,3,2);
		$tahun = substr($tgl,6,4);
		return $tahun."-".$bulan."-".$tanggal;
	}	
	// tanggal -bulan -tahun
	public static function tgl1($tgl)
	{
		$tanggal = substr($tgl,8,2);
		$bulan = substr($tgl,5,2);
		$tahun = substr($tgl,0,4);
		$tgl = $tanggal."-".$bulan."-".$tahun;	

		if ($tgl != "--" ) {
			return $tanggal."-".$bulan."-".$tahun;		    		
		}
	}

	public static function bln($tgl)
	{ 
		$bulan = Helper::getBulan(substr($tgl,5,2)); 
		return $bulan;
	}	

	public static function thn($tgl)
	{ 
		$tahun = substr($tgl,0,4); 
		return $tahun;
	}

	public static function getBulan($bln)
	{
		switch ($bln){ 
			case 1:	return "Januari";  break; 
			case 2: return "Februari";  break; 
			case 3: return "Maret";  break;
			case 4: return "April"; break;
			case 5: return "Mei";  break; 
			case 6: return "Juni"; break; 
			case 7:	return "Juli"; break;
			case 8:	return "Agustus"; break; 
			case 9:	return "September"; break;
			case 10:return "Oktober"; break;
			case 11:return "November"; break;
			case 12:return "Desember"; break; 
		} 
	}

	public static function auto(){
		// $v = Pencairan::select('pencairan_id')->orderBy('pencairan_id','desc')->first();
		// $no = isset($v) ? $v->pencairan_id : 0;
		// return $no+1;
	}

	// fungsi upload gambar produk
	public static function uploadImage($image, $folder, $fileold){    
		$tgl = date('Y-m-d');
	    $file = array('file' => $image); 
	    $rules = array('file' => 'mimes:jpeg,jpg,gif,png');
	    $validator = Validator::make($file, $rules);

	    if ($validator->fails() or  $image == NUll) { 
        	$fileName = $fileold=='' ? '' : $fileold; 
	    } else {  
	        $extension = strstr($image->getClientOriginalName(), '.');
	        $uniq = uniqid();
	        $fileName = $tgl."-".$uniq.$extension;
	        $fileName = str_replace('-','',$fileName);

	        $image->move($folder, $fileName);
	        // list($w, $h) = getimagesize($folder.$fileName);
	        // $w = $w / 2;
	        // $h = $h / 2;            
	        //  // open an image file
	        // $img_medium = Image::make($folder.$fileName);
	        // // resize image instance
	        // $img_medium->resize($w, $h);
	        // // save image in desired format
	        // $img_medium->save($folder."medium/".$fileName);

	        // $w = $w / 2;
	        // $h = $h / 2;   
	        //  // open an image file
	        // $img_small = Image::make($folder.$fileName);
	        // // resize image instance
	        // $img_small->resize($w, $h);
	        // // save image in desired format
	        // $img_small->save($folder."small/".$fileName);

        	Helper::DeleteImage($fileold, $folder);
	    }
	    return $fileName;
	}	
	// delete foto produk
	public static function deleteImage($image, $folder){
	    File::delete($folder.$image); 
	    // File::delete($folder."medium/".$image); 
	    // File::delete($folder."small/".$image); 
	}

	public static function uploadFile($image, $path, $file_old){
		$tgl = date('Y-m-d');
		$file = array('file' => $image); 
        $rules = array('file' => 'mimes:pdf');
	    $validator = Validator::make($file, $rules);

	    if ($validator->fails() or  $image == NUll) { 
	        $fileName = $file_old=='' ? '' : $file_old; 
	    } else {  
	        $extension = strstr($image->getClientOriginalName(), '.');
	        $uniq = uniqid();
	        $fileName = $tgl."_".$uniq.$extension; 
	        $image->move($path, $fileName);
	        Helper::deleteFile($file_old, $path);
		}
    	return $fileName;
	}

	public static function deleteFile($image, $path){
    	File::delete($path.$image);  
	}

	// get Status
	public static function getStatus($id){
		$v = DB::table('abupay-passport.status')->select('deskripsi')->where('kode_status', $id)->first();
		return isset($v) ? $v->deskripsi : '';
	}

	public static function bank(){
		$v = DB::table('bank')->select('kode_bank')->orderBy('kode_bank','desc')->first();
		if (isset($v)){
			$no = (int) substr($v->kode_bank, -3);
			$no++;
		}else{
			$no=1;
		}
		return "BK".sprintf("%03s",$no);
	}

	public static function kategori(){
		$v = DB::table('kategori')->select('kategori_name')->orderBy('kategori_name','asc')->get();
		return isset($v) ? $v : '';
	}
	
	public static function Opt($id) {
		$model = DB::table('opt')->select('description')->where('option', '=', $id)->get();		
    	return isset($model) ? $model : "";  
	}

	public static function noRegister($tabel, $kode){
		$v  = DB::table($tabel)->select('no_reg', 'tgl_masuk')->where('no_reg','<>','')->orderBy('id','desc')->first();
		$no = isset($v->no_reg) ? $v->no_reg : '' ;
		$tgl  = isset($v->tgl_masuk) ? $v->tgl_masuk : '' ;
    	if ( substr($tgl,0,4) == date('Y') ) {
    		$no = (int) substr($no, -4); 
	        $no++;
	    }else{
	        $no = "0001";
	    }      
		return $kode."-".sprintf("%04s", $no);  
	}
}


